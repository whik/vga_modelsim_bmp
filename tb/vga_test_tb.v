`timescale 1ns/100ps

module vga_test_tb;

parameter PERIOD = 10;

reg clk_vga;
reg rst_n;
reg [3:0] vga_dis_mode;

wire vga_hsync;
wire vga_vsync;
wire [15:0] vga_data;

always #(PERIOD/2) clk_vga = !clk_vga;

vga_test vga_test(
    //Inputs
     .clk(clk_vga)
    ,.rst_n(rst_n)
    ,.vga_dis_mode(vga_dis_mode[3:0])

    //Outputs
    ,.vga_hsync(vga_hsync)
    ,.vga_vsync(vga_vsync)
    ,.vga_data(vga_data[15:0])
);

bmp_gen bmp_gen_ut0(
     //Inputs
     .clk_vga(clk_vga)
    ,.rst_n(rst_n)
    ,.vga_hsync(vga_hsync)
    ,.vga_vsync(vga_vsync)
    ,.vga_data(vga_data[15:0])
);

always @ (negedge vga_vsync) begin
    if(!rst_n)
        vga_dis_mode <= 0;
    else if(vga_dis_mode == 13)
        $stop;
    else
        vga_dis_mode <= vga_dis_mode + 1;
end

initial begin
    rst_n = 0;
    clk_vga = 0;
    vga_dis_mode = 0;
    #200

    @(posedge clk_vga)
    rst_n = 1;
end

endmodule
