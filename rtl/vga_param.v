 /*
    1024 x 768 @ 60Hz
    65MHz, 
    H: 236+160+1024+24=1344
    V: 6+29+768+3=806
*/
/*
     800x480 640x480 800x600  800x600  1024x768 1024x768 1280x1024 1280x800 1440x900
a     96     64      128      80       136      176      112       136      152 
b     48     120     88       160      160      176      248       200      232 
c     640    640     800      800      1024     1024     1280      1280     1440
d     16     16      40       16       24       16       48        64       80  
e     800    480     1056     1056     1344     1312     1688      1680     1904
o     2      3       4        3        6        3        3         3        3   
p     33     16      23       21       29       28       38        24       28  
q     480    480     600      600      768      768      1024      800      900 
r     10     1       1        1        3        1        1         1        1   
s     525    500     628      625      806      800      1066      828      932 

fps   60     75      60       75       60       75       60        60       60
clk   25.175 31.5    40       49.5     65       78.8     108       83.46    106.47};
*/

// `define BMP_GEN_10X5_SIM

`ifdef BMP_GEN_10X5_SIM
    `define VGA_HA   4
    `define VGA_HB   4 
    `define VGA_HC   10
    `define VGA_HD   4

    `define VGA_VO   4   
    `define VGA_VP   4  
    `define VGA_VQ   5
    `define VGA_VR   3   
`else
    `define VGA_HA  96  
    `define VGA_HB  48  
    `define VGA_HC  640 
    `define VGA_HD  16  
    `define VGA_HE  800 
    `define VGA_VO  2   
    `define VGA_VP  33  
    `define VGA_VQ  480 
    `define VGA_VR  10  
    `define VGA_VS  525 
`endif

`define VGA_HE     (`VGA_HA + `VGA_HB + `VGA_HC + `VGA_HD)
`define VGA_VS     (`VGA_VO + `VGA_VP + `VGA_VQ + `VGA_VR)
`define VGA_WIDTH  `VGA_HC
`define VGA_HEIGHT `VGA_VQ
