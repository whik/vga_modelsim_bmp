
module vga_test(
     //Inputs
     input clk
    ,input rst_n
    ,input [3:0] vga_dis_mode
    
     //Outputs
    ,output vga_hsync
    ,output vga_vsync
    ,output [15:0] vga_data
);

wire [15:0] rgb_data;
wire [11:0] pos_x;    //0 ~ WIDTH  - 1
wire [11:0] pos_y;    //0 ~ HEIGHT - 1

vga_driver vga_driver_ut0(
     //Inputs
     .clk(clk)
    ,.rgb_data(rgb_data[15:0])
    ,.rst_n(rst_n)
    
    //Outputs
    ,.pos_x(pos_x[11:0])  //0 ~ WIDTH  - 1
    ,.pos_y(pos_y[11:0])  //0 ~ HEIGHT - 1
    ,.vga_data(vga_data[15:0])
    ,.vga_hsync(vga_hsync)
    ,.vga_vsync(vga_vsync)
);

vga_display_ctrl vga_display_ctrl_ut0(
    //Inputs
     .clk(clk)
    ,.rst_n(rst_n)
    ,.pos_x(pos_x[11:0])   //0 ~ WIDTH  - 1
    ,.pos_y(pos_y[11:0])   //0 ~ HEIGHT - 1
    ,.vga_dis_mode(vga_dis_mode[3:0])
    
    //Outputs
    ,.rgb_data(rgb_data[15:0])
);

endmodule